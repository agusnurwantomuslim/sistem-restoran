<?php 
#recursive algoritm for looping tree
function tree($ug,$td_ug,$data, $parent = 0,$jalur_ortu='0,') {
	//global $ug;
	//global $td_ug;
	static $i = 0;
	static $j = 0;
	$tab = str_repeat("", $i);
	//$tab = str_repeat("&nbsp;&nbsp;", $i);
	$padding=($i*10).'px';
	if (isset($data[$parent])) {
		$html = "";
		$i++;
		foreach ($data[$parent] as $v) {
			$jalur_ortu.="$v[id],";
			$child = tree($ug,$td_ug,$data, $v['id'],$jalur_ortu);
			$jalur_ortu=str_replace("$v[id],","",$jalur_ortu);
			if($v['acl']=='Y'){
				$checked="checked";
				$plus="<td></td>";
			}else{
				##tombol plus
				$checked='';$plus="
				<td style='cursor:pointer;' onclick=\"$('.tr_baru').remove();
					$('#div_$v[id]').append('<tr class=tr_baru ><td></td>'+
						'<td style=padding-left:$padding; >$tab &nbsp;&nbsp;<input type=text style=margin-bottom:0px class=input-medium name=nama_$v[id] /></td>'+
						
						'<td><input type=text class=input-medium style=margin-bottom:0px name=label_$v[id] /></td>'+
						'<td style=text-align:center; ><input name=acl_$v[id] type=checkbox value=Y ></td>'+
						'<td></td>'+
						'".str_replace("usergroup","usergroup_$v[id]",$td_ug)." '+
						'<td><input type=button value=ok onclick=save_acl(\'$v[id]\',\'".($v['lefel']+1)."\',\'$jalur_ortu\') /></td>'+
						'</tr>');\">
					<i class='icon-plus'></i>
				</td>";
			}
			#body table
			if ($v['acl']==='N') {
				$modul_nama="<a href='".BASE_URL."modul/$v[url]'>$v[modul_nama]</a>";
			}else{
				$modul_nama="<a href=# >".$v['modul_nama']."</a>";
			}
			$html .= "<tbody id='div_$v[id]' >
				<tr >
					$plus
					<td nowrap id='td_nama_$v[id]' style='padding-left:$padding;'>$tab <i class='$v[icon]'> </i> $modul_nama</td>
					
					<td class='td_input' id='td_label_$v[id]'  >
						<span >$v[modul_label]</span></td>
					<td align=center style=text-align:center; >
						<a title='Acl for $v[modul_nama]' data-rel='tooltip' >
							<input id=$v[id] type=checkbox $checked value=Y >
						</a>
					</td>
					<td>
						<a style='float:right;' href='#' title='Remove' data-rel='tooltip' onclick=\"return del_acl('$v[id]');\" >
							<i class='icon-remove'></i>
						</a> 
						<a style='float:right;' href='#' title='Edit' data-rel='tooltip' 
							onclick=\"edit_form('$v[id]',$('#td_nama_$v[id] i').attr('class'),$('#td_nama_$v[id] a').html(),$('#td_label_$v[id] span').html(),'$v[acl]','$v[parent]',$('#td_nama_$v[id] a').attr('href'),'$v[urutan]','$v[lefel]','$v[menugroup]');\" >
							<i class='icon-pencil'></i>
						</a>
					</td>
					";
				$ug_acl=explode(",",$v['id_usergroup']);
				 #_______________________
				#/ kolom usergroup \___________________________________________________________________________	
				foreach ($ug as $k1=>$v1) {
					$checked="";$akses='N';
					if (in_array($v1['id'],$ug_acl)) {$checked="checked";$akses='Y';}
					$html.="<td align=center  style=text-align:center;  >
						<a title='Access $v[modul_nama] for $v1[nama]' data-rel='tooltip' >
							<input $checked id=".$v['id']."_$v1[id] type=checkbox value=Y onclick=\"ajax_upd_user_acl($v[id],$v1[id],'$akses');\">
						</a>
					</td>";
				}
				#__________________________________________________________________________________________
			$html .= "</tr></tbody>";
			if ($child) {$i--;$html .= $child;}
			$html .= '';
		}
		$html .= "";
		return $html;
	} else {
		return false;
	}
}
function view_form_edit($data){
	$a="<div id='form_edit' class='modal hide fade' style='display:none;'>
			<form id='form_edit1'>
			<div class='modal-header'>
				<button data-dismiss='modal' class='close' type='button'>×</button>
				<h3>Settings</h3>
			</div>
			<div class='modal-body'>
				
				<table class='table' style=''>
					<tr>
						<td>Icon</td><td>: <input type='text' class='input-small' name='icon' value='' id='form_icon' /></td>
					</tr>
					<tr>
						<td>Modul Nama</td><td>: <input type='text' class='input-medium' id='form_modul_nama' name='modul_nama' value='' /></td>
					</tr>
					<tr>
						<td>Modul Label</td><td>: <input type='text' class='input-medium' id='form_modul_label' name='modul_label' value='' /></td>
					</tr>
					<tr>
						<td>ACL</td><td>: 
						<select style='width:50px;' name='acl' id='form_acl'>
									<option value=Y >Y</option>
									<option value=N >N</option>
							</select>
						</td>
					</tr>
					<tr>
						<td>Parent</td><td>: 
							<select name='parent' id=parent>
								<option value=0 >root</option>
									$data[dropdown_parent]
							</select>
						</td>
					</tr>
					<tr>
						<td>Url</td><td>: <input type='text' class='input-large' id='form_url' name='url' value='' id='' /></td>
					</tr>
					<tr>
						<td>Sort</td><td>: <select id='urutan' style='width:50px;' name='urutan'>
									$data[angka1sd10]
							</select></td>
					</tr>
					<tr>
						<td>Level Child</td><td>: <select style='width:50px;' id='lefel' name='lefel'>
									$data[angka1sd10]
							</select></td>
					</tr>
					<tr>
						<td>Menu Group</td><td>: <input type='text' class='input-small' name='menugroup' value='' id='' /></td>
					</tr>
				</table>
					<input type='hidden' name='nav' value='update_acl' />
				
			</div>
			<div class='modal-footer'>
				<input type='hidden' name='parent_lama' value='' />
				<input type='hidden' name='id' value='' id='hidden_id' />
				<a data-dismiss='modal' id=update_acl_button_close class='btn' href='#'>Close</a>
				<a id='update_acl_button' onclick=\"ajax_upd_acl($('#hidden_id').val());\" class='btn btn-primary' href='#'>Save changes</a>
			</div>
			</form>
		</div>";
	return $a;
}

function view_default($data){
	foreach ($data['ug'] as $k=>$v) {
		$ug1[$v['id']]=$v['nama'];
	}

	unset($k);unset($v);
	if (!empty($data['acl'])) {
		foreach ($data['acl'] as $k=>$v) {
			$acl1[$v['parent']][]=$v;
		}	
	}

	foreach ($data['ug'] as $k1=>$v1) {
		//@$td_ug.="<td align=center ><select id=usergroup_$v1[id] ><option value=N >N</option><option value=Y >Y</option></select></td>";
		@$td_ug.="<td align=center style=text-align:center; ><input id=usergroup_$v1[id] type=checkbox value=Y ></td>";
	}
	
	#/ view table luar acl \___________________________________________________________________________	
	$tabel=view_form_edit($data)."
	
	<script type='text/javascript'>
		function ajax_upd_user_acl(id,id_usergroup,akses){
			if (akses=='Y') {
				var jadi='N';
			}else{
				var jadi='Y';
			}
			$.ajax({
				url: '".d_link('acl')."',
				type: 'POST',
				dataType: 'html',
				data: 'nav=update_user_acl&acl='+jadi+'&id='+id+'&id_usergroup='+id_usergroup,
				success: function(obj){
					$('input[type=checkbox]#'+id+'_'+id_usergroup).attr('onclick','ajax_upd_user_acl('+id+','+id_usergroup+',\''+jadi+'\');');
					//alert(obj);
					$('#btn_notifikasi').click();
				}
			});
		}
		function ajax_upd_acl(id){
			$.ajax({
				url: '".d_link('acl')."',
				type: 'POST',
				dataType: 'html',
				data: $('form#form_edit1').serialize(),
				success: function(obj){
					$('#td_label_'+id).html('<span>'+$('#form_modul_label').val()+'</span>');
					$('#td_nama_'+id+' i').attr('class',$('#form_icon').val());
					$('#td_nama_'+id+' a').html($('#form_modul_nama').val());
					$('#td_nama_'+id+' a').attr('href',$('#form_url').val());
					$('#update_acl_button_close').click();
					if (obj=='redirect') {
						window.location.replace('acl_c.aspx');
					}
				}
			});
		}
		function edit_form(id,icon,modul_nama,modul_label,acl,parent,url,urutan,lefel,menugroup){
			$('input[name=id]').val(id);
			$('input[name=icon]').val(icon);
			$('input[name=modul_nama]').val(modul_nama);
			$('input[name=parent_lama]').val(parent);
			$('input[name=modul_label]').val(modul_label);
			$('select#acl option[value='+acl+']').attr('selected','selected');
			$('select#parent option[value='+parent+']').attr('selected','selected');
			$('input[name=url]').val(url);
			$('select#urutan option[value='+urutan+']').attr('selected','selected');
			$('select#lefel option[value='+lefel+']').attr('selected','selected');
			$('input[name=menugroup]').val(menugroup);
			$('#form_edit').modal('show');
		}
		
		function save_acl(id,level,jalur){
			var nama=$('input[name=nama_'+id+']').val();
			var label=($('input[name=label_'+id+']').val());
			var acl=($('input[name=acl_'+id+']').prop('checked'));
			var usergroup = new Array();";
			foreach ($data['ug'] as $k1=>$v1) {
				$tabel.=" usergroup[$v1[id]]='$v1[id]_'+($('#usergroup_'+id+'_$v1[id]').prop('checked'));";
			}
			$tabel.="
			$(function(){
				$.ajax({
					url: '".d_link('acl')."',
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: 'nav=save_acl&id='+id+'&jalur='+jalur+'&nama='+nama+'&label='+label+'&acl='+acl+
						'&level='+level+'&parent='+id+'&usergroup='+usergroup.toString(),
					success: function(obj){
						$('.tr_baru').remove();
						if(id == 0){
							$('table').prepend(obj);
						}else{
							$('#div_'+id).after(obj);
						}
						//$('table').append(obj);
					}
				});
			});
		}
		
		function del_acl(id){
			if (confirm('sub dari menu ini juga akan terhapus... Anda yakin?')) {
				$.ajax({
					url: '".d_link('acl')."',
					cache: false,
					type: 'POST',
					dataType: 'html',
					data: 'nav=del_acl&id='+id,
					success: function(obj){
						var pecah=obj.split(',');
						for (key in pecah){
							$('#div_'+pecah[key]).fadeOut(
								function(){
									$(this).remove();
								});
						}
					}
				});
				return false;
			}
		}
	</script>
	<button id='btn_notifikasi' style='display:none;' class=\"btn btn-primary noty\" 
		data-noty-options='{\"text\":\"Update Success.\",\"layout\":\"topRight\",\"type\":\"success\"}'>
		<i class=\"icon-bell icon-white\"></i> Top Right
	</button>
	<table class='table' >
		<thead>
			<tr>
				<th rowspan=2 style=width:20px; onclick=\"$('.tr_baru').remove();$('thead').append('<tr class=tr_baru ><td></td>'+
					'<td><input type=text style=margin-bottom:0px class=input-medium name=nama_0  /> </td>'+
					'<td><input type=text style=margin-bottom:0px class=input-medium name=label_0  /></td>'+
					'<td style=text-align:center; ><input name=acl_0 style=display:none; type=checkbox value=Y ></td>'+
					'<td></td>'+
					'".str_replace("usergroup","usergroup_0",$td_ug)."'+
					'<td><input type=button value=ok onclick=save_acl(\'0\',\'1\',\'0\') /></td></tr>');\" >
					+
				</th>
				<th rowspan=2 style=width:180px; nowrap >Menu/Submenu/ACL</th>
				
				<th rowspan=2 style=width:170px; nowrap >Label Menu</th>
				
				<th rowspan=2 style=text-align:center; >ACL</th>
				<th rowspan=2  ></th>
				<th colspan=".count($data['ug'])." style=text-align:center; >Usergroup</th>
			</tr>
			<tr>";
				if (!empty($data['ug'])) {
					unset($k);unset($v);
					foreach ($data['ug'] as $k=>$v) {
						$tabel.="<th style=text-align:center; >$v[nama]</th>";
					}
				}
	$tabel.="</tr>
		</thead>
		";
		if (!empty($acl1)) {
			$tabel.=tree($data['ug'],$td_ug,$acl1,0);
		}	
	$tabel.="
	</table>
	";

	#div diluar table
	$a="<div class='row-fluid sortable'>
				<div class='box span12'>
					<div class='box-header well' data-original-title>
						<h2><i class='icon-picture'></i>Tabel Hak Akses</h2>
						<div class='box-icon'>
							<a href='#' class='btn btn-setting btn-round'><i class='icon-cog'></i></a>
							<a href='#' class='btn btn-minimize btn-round'><i class='icon-chevron-up'></i></a>
							<a href='#' class='btn btn-close btn-round'><i class='icon-remove'></i></a>
						</div>
					</div>
					<div class='box-content' style='overflow:auto;'>
						<!--mulai table acl-->
						$tabel
						<!--selesai -->
					</div>
				</div><!--/span-->
			
			</div><!--/row-->

				
					<!-- content ends -->
			</div><!--/#content.span10-->
				</div><!--/fluid-row-->
		<hr>

		<div class='modal hide fade' id='myModal'>
			<div class='modal-header'>
				<button type='button' class='close' data-dismiss='modal'>×</button>
				<h3>Settings</h3>
			</div>
			<div class='modal-body'>
				<p>Here settings can be configured...</p>
			</div>
			<div class='modal-footer'>
				<a href='#' class='btn' data-dismiss='modal'>Close</a>
				<a href='#' class='btn btn-primary'>Save changes</a>
			</div>
		</div>

		<footer>
			<p class='pull-left'>&copy; <a href='http://usman.it' target='_blank'>Muhammad Usman</a> 2013</p>
			<p class='pull-right'>Powered by: <a href='http://usman.it/free-responsive-admin-template'>Charisma</a></p>
		</footer>
		
	</div><!--/.fluid-container-->
	";
	return $a;
}

function view_acl_exist($dt=''){
	echo "<tbody id=div_$dt[id] >
			<tr>
				<td colspan=100 style='color:red;padding-left:40px;'>
				<a href='#' onclick=$('#div_$dt[id]').remove(); >x</a>
				Data Exist.</td>
			</tr>
		</tbody>";
}
function view_save_acl($dt=''){
		foreach ($dt['ug_from_db'] as $k1=>$v1) {
			//@$td_ug.="<td align=center ><select id=usergroup_$v1[id] ><option value=N >N</option><option value=Y >Y</option></select></td>";
			@$dt['td_ug'].="<td align=center style=text-align:center; ><input id=usergroup_$v1[id] type=checkbox value=Y > </td>";
			$checked="";
			if (!empty($dt['ug_from_post'])) {
				if (in_array($v1['id'],$dt['ug_from_post'])){$checked="checked";}
			}
			@$checkbox.="<td align=center  style=text-align:center;  >
					<a title='Access $v[modul_nama] for $v1[nama]' data-rel='tooltip' >
						<input $checked id=$v1[id] type=checkbox value=Y >
					</a>
				</td>";
		}
		echo "<tbody id=div_$dt[id] >
			<tr>
				<td onclick=\"$('.tr_baru').remove();
					$('#div_$dt[id]').append('<tr class=tr_baru ><td>*</td>'+
						'<td>$dt[tab] &nbsp;&nbsp;&nbsp;<input type=text style=margin-bottom:0px class=input-medium name=nama_$dt[id] /></td>'+
						'<td></td>'+
						'<td><input type=text class=input-medium style=margin-bottom:0px name=label_$dt[id] /></td>'+
						'<td style=text-align:center; ><input name=acl_$dt[id] type=checkbox value=Y ></td>'+
						'".str_replace("usergroup","usergroup_$dt[id]",$dt['td_ug'])." '+
						'<td><input type=button value=ok onclick=save_acl(\'$dt[id]\',\'".($dt['lefel']+1)."\',\'$dt[jalur_parent],\') /></td>'+
						'</tr>');\">
					+
				</td>
				<td>$dt[tab] ".$dt['modul_nama']."</td>
				<td>
					<span style='float:right;'><a href='#' onclick=del_acl('$dt[id]') >x</a></span>
				</td><td>$dt[modul_label]</td>
				";
				if ($dt['acl']=='Y') {$checked="checked";}else{$checked="";}
				echo "<td align=center  style=text-align:center;  >
					<a title='Acl for $dt[modul_nama] ' data-rel='tooltip' >
						<input $checked id=$dt[id] type=checkbox value=Y >
					</a>
				</td>$checkbox";
				
			echo "</tr></tbody>";
}
?>
