<?php 
/*sponsored by jasaprogrammer.com (ztoro code team)*/

function view_default($dt=''){
	//return $dt['content'];
	$a = "
		<script>
			function ajax_new_user(){
				$.ajax({
					url: '".d_link('add_user')."',
					type: 'POST',
					dataType: 'html',
					data: $('form#form_new_user').serialize(),
					success: function(obj){
						//alert(obj);
						$('.min_add_user').click();
					}
				});
			}
			function validation_uid(){ 
				var usr = $(\"#user_name\").val();
				if(usr.length >= 4){
					$.ajax({  
						type: \"POST\",  
						url: '".d_link('add_user')."',  
						data: \"username=\"+ usr,  
						success: function(msg){
							//alert(msg);
							if(msg == 'OK'){
								document.getElementById(\"uid\").className = \"control-group left success\";
								$('#status').hide(); 
							}else{  
								document.getElementById(\"uid\").className = \"control-group left error\"; 
								$('#status').show();
								$('#status').html(msg); 
							}  
						}
					});
				}else{
					document.getElementById(\"uid\").className = \"control-group left error\";
					$('#status').show();
					$('#status').html('<font color=red >Karakter harus lebih dari 3 huruf</font>');
				} 
			}
			function validation_email(){ 
				var mail = $(\"#email\").val();
				//alert (mail);
				var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
				if( re.test(mail)==true ){
					document.getElementById(\"mail\").className = \"control-group left success\";
					$('#status_m').hide(); 
				}else{  
					document.getElementById(\"mail\").className = \"control-group left error\"; 
					$('#status_m').show();
					$('#status_m').html('<font color=red >Not correct email</font>'); 
				} 
			}
			function cekNumerik(){
			  // membaca nilai dari input form (komponen bernama 'data'), dan disimpan sebagai x
			  var x = document.form1.data.value;

			  // membuat daftar karakter '0' s/d '9' dalam array
			  var list = new Array('0', '1', '2', '3', '4', '5', '6', '7', '8', '9');

			  // nilai awal status
			  var status = true;

			  // proses pengecekan setiap karakter dalam string
			  // looping dilakukan sebanyak jumlah karakter dalam string
			  for (i=0; i<=x.length-1; i++){
				 // jika karakter ke-i termasuk dalam array, maka nilainya TRUE
				 // sedang jika tidak, nilai FALSE
				 if (x[i] in list) cek = true;
				 else cek = false;

				 // kenakan operasi AND
				 status = status && cek;
			  }
			  if (status == false){ 
				  // jika status akhir bernilai FALSE maka munculkan kotak peringatan
				 // dan akan mengembalikan nilai FALSE

				 alert('Bukan angka');
				 return false;
			  }else{
				 // sedang jika nilai status akhir TRUE, maka akan mengembalikan nilai TRUE
				 return true;
			  }
			}
		</script>
		<style>
			.clear{
				clear:both;
			}
			.left{
				float:left;
			}
			.right{
				float:right;
			}
		</style>
			<div class=\"row-fluid sortable\">
				<div class=\"box span12\">
					<div class=\"box-header well\" data-original-title>
						<h2><i class=\"icon-edit\"></i> Add User </h2>
						<div class=\"box-icon\">
							<a href=\"#\" class=\"btn btn-setting btn-round\"><i class=\"icon-cog\"></i></a>
							<a href=\"#\" class=\"btn btn-minimize btn-round min_add_user\"><i class=\"icon-chevron-up\"></i></a>
							<a href=\"#\" class=\"btn btn-close btn-round\"><i class=\"icon-remove\"></i></a>
						</div>
					</div>
					<div class=\"box-content\">
						<form class=\"form-horizontal\" id='form_new_user'>
							<fieldset>
							  <div class='clear'></div>
							  <div id='uid' class=\"control-group left\">
								<label class=\"control-label\" for=\"user_name\">User Name :</label>
								<div class=\"controls\">
								  <input id=\"user_name\" type=\"text\" value=\"\" name='user_name' onchange='validation_uid()' >
								<div id='status'></div>
								</div>
							  </div>
							  <div class=\"control-group left\">
								<label class=\"control-label\" for=\"nickname\">Nama Panggilan :</label>
								<div class=\"controls\">
								  <input id=\"nickname\" type=\"text\" value=\"\" name='nickname'>
								</div>
							  </div>
							  <div class='clear'></div>
							  <div class=\"control-group left\">
								<label class=\"control-label\" for=\"fullname\">Nama Lengkap :</label>
								<div class=\"controls\">
								  <input id=\"fullname\" type=\"text\" value=\"\" name='fullname'>
								</div>
							  </div>
							  <div class=\"control-group left\">
								<label class=\"control-label\" for=\"ttl\">Tempat, Tanggal Lahir :</label>
								<div class=\"controls\">
								  <input id=\"ttl\" type=\"text\" value=\"\" name='ttl'>
								</div>
							  </div>
							  <div class='clear'></div>
							  <div id='mail' class=\"control-group left\">
								<label class=\"control-label\" for=\"email\">Email :</label>
								<div class=\"controls\">
								  <input id=\"email\" type=\"text\" value=\"\" name='email' onchange='validation_email()' >
								  <div id='status_m'></div>
								</div>
							  </div>
							  <div class=\"control-group left\">
								<label class=\"control-label\" for=\"facebook\">Facebook :</label>
								<div class=\"controls\">
								  <input id=\"facebook\" type=\"text\" value=\"\" name='facebook'>
								</div>
							  </div>
							  <div class='clear'></div>
							  <div class=\"control-group left\">
								<label class=\"control-label\" for=\"twitter\">Twitter :</label>
								<div class=\"controls\">
								  <input id=\"twitter\" type=\"text\" value=\"\" name='twitter'>
								</div>
							  </div>
							  <div class=\"control-group left\">
								<label class=\"control-label\" for=\"tlp\">No Tlp :</label>
								<div class=\"controls\">
								  <input id=\"tlp\" type=\"text\" value=\"\" name='tlp'>
								</div>
							  </div>
							  <div class='clear'></div>
							  <div class=\"control-group\">
								<label class=\"control-label\" for=\"alamat\">Alamat :</label>
								<div class=\"controls\">
									<textarea name='alamat' id='alamat'></textarea>
								</div>
							  </div>
							  <div class='clear'></div>
							  <div class=\"control-group left\">
								<label class=\"control-label\">Gender :</label>
								<div class=\"controls\">
								  <label class=\"radio\">
									<input type=\"radio\" name=\"gender\" id=\"optionsRadios1\" value=\"M\" checked >
									Male
								  </label>
								  <div style=\"clear:both\"></div>
								  <label class=\"radio\">
									<input type=\"radio\" name=\"gender\" id=\"optionsRadios2\" value=\"F\">
									Female
								  </label>
								</div>
							  </div>
							  <div class=\"control-group left\">
								<label class=\"control-label\">Status :</label>
								<div class=\"controls\">
								  <label class=\"radio\">
									<input type=\"radio\" name=\"status\" id=\"optionsRadios1\" value=\"option1\" checked >
									Married
								  </label>
								  <div style=\"clear:both\"></div>
								  <label class=\"radio\">
									<input type=\"radio\" name=\"status\" id=\"optionsRadios2\" value=\"option2\">
									Single
								  </label>
								</div>
							  </div>
							  <div class='clear'></div>
							  <div class=\"control-group left\">
								<label class=\"control-label\" for=\"selectError\">User Group :</label>
								<div class=\"controls\">
								  <select id=\"selectError\" data-rel=\"chosen\" name'user_group'>
									<option value='admin'>Admin</option>
									<option value='boss'>Boss</option>
									<option value='witter'>Witter</option>
									<option value='kitchen'>Kitchen</option>
									<option value='kasir'>Kasir</option>
								  </select>
								</div>
							  </div>
							  <div class=\"control-group left\">
								<label class=\"control-label\">Upload Photo :</label>
								<div class=\"controls\">
								  <input type=\"file\">
								</div>
							  </div>
							  <div class='clear'></div>
							  <div class=\"control-group left\">
								<label class=\"control-label\" for=\"password1\">Password :</label>
								<div class=\"controls\">
								  <input id=\"password1\" type=\"password\" value=\"\" name='password1'>
								</div>
							  </div>
							  <div class=\"control-group left\">
								<label class=\"control-label\" for=\"password2\">Retry Password :</label>
								<div class=\"controls\">
								  <input id=\"password2\" type=\"password\" value=\"\" name='password2'>
								</div>
							  </div>
							  <div class='clear'></div>
							  <div class=\"form-actions\">
							    <input type='hidden' name='nav' value='new_user'>
							    <a id='update_acl_button' onclick=\"ajax_new_user();\" class='btn btn-primary' href='#'>Save changes</a>
								<button class=\"btn\">Cancel</button>
							  </div>
							</fieldset>
						  </form>
					
					</div>
				</div><!--/span-->
			
			</div><!--/row-->
			<div class=\"row-fluid sortable\">		
				<div class=\"box span12\">
					<div class=\"box-header well\" data-original-title>
						<h2><i class=\"icon-user\"></i> Members</h2>
						<div class=\"box-icon\">
							<a href=\"#\" class=\"btn btn-setting btn-round\"><i class=\"icon-cog\"></i></a>
							<a href=\"#\" class=\"btn btn-minimize btn-round\"><i class=\"icon-chevron-up\"></i></a>
							<a href=\"#\" class=\"btn btn-close btn-round\"><i class=\"icon-remove\"></i></a>
						</div>
					</div>
					<div class=\"box-content\">
						<table class=\"table table-striped table-bordered bootstrap-datatable datatable\">
						  <thead>
							  <tr>
								  <th>Username</th>
								  <th>Date registered</th>
								  <th>Role</th>
								  <th>Status</th>
								  <th>Actions</th>
							  </tr>
						  </thead>   
						  <tbody>
							<tr>
								<td>David R</td>
								<td class=\"center\">2012/01/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-success\">Active</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Chris Jack</td>
								<td class=\"center\">2012/01/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-success\">Active</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Jack Chris</td>
								<td class=\"center\">2012/01/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-success\">Active</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Muhammad Usman</td>
								<td class=\"center\">2012/01/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-success\">Active</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Sheikh Heera</td>
								<td class=\"center\">2012/02/01</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-important\">Banned</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Helen Garner</td>
								<td class=\"center\">2012/02/01</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-important\">Banned</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Saruar Ahmed</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Ahemd Saruar</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Habib Rizwan</td>
								<td class=\"center\">2012/01/21</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-success\">Active</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Rizwan Habib</td>
								<td class=\"center\">2012/01/21</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-success\">Active</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Amrin Sana</td>
								<td class=\"center\">2012/08/23</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-important\">Banned</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Sana Amrin</td>
								<td class=\"center\">2012/08/23</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-important\">Banned</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Ifrah Jannat</td>
								<td class=\"center\">2012/06/01</td>
								<td class=\"center\">Admin</td>
								<td class=\"center\">
									<span class=\"label\">Inactive</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Jannat Ifrah</td>
								<td class=\"center\">2012/06/01</td>
								<td class=\"center\">Admin</td>
								<td class=\"center\">
									<span class=\"label\">Inactive</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Robert</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Dave Robert</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Brown Robert</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Usman Muhammad</td>
								<td class=\"center\">2012/01/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-success\">Active</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Abdullah</td>
								<td class=\"center\">2012/02/01</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-important\">Banned</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Dow John</td>
								<td class=\"center\">2012/02/01</td>
								<td class=\"center\">Admin</td>
								<td class=\"center\">
									<span class=\"label\">Inactive</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>John R</td>
								<td class=\"center\">2012/02/01</td>
								<td class=\"center\">Admin</td>
								<td class=\"center\">
									<span class=\"label\">Inactive</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Paul Wilson</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Wilson Paul</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Heera Sheikh</td>
								<td class=\"center\">2012/01/21</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-success\">Active</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Sheikh Heera</td>
								<td class=\"center\">2012/01/21</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-success\">Active</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Christopher</td>
								<td class=\"center\">2012/08/23</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-important\">Banned</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Andro Christopher</td>
								<td class=\"center\">2012/08/23</td>
								<td class=\"center\">Staff</td>
								<td class=\"center\">
									<span class=\"label label-important\">Banned</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Jhon Doe</td>
								<td class=\"center\">2012/06/01</td>
								<td class=\"center\">Admin</td>
								<td class=\"center\">
									<span class=\"label\">Inactive</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Lorem Ipsum</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Abraham</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Brown Blue</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
							<tr>
								<td>Worth Name</td>
								<td class=\"center\">2012/03/01</td>
								<td class=\"center\">Member</td>
								<td class=\"center\">
									<span class=\"label label-warning\">Pending</span>
								</td>
								<td class=\"center\">
									<a class=\"btn btn-success\" href=\"#\">
										<i class=\"icon-zoom-in icon-white\"></i>  
										View                                            
									</a>
									<a class=\"btn btn-info\" href=\"#\">
										<i class=\"icon-edit icon-white\"></i>  
										Edit                                            
									</a>
									<a class=\"btn btn-danger\" href=\"#\">
										<i class=\"icon-trash icon-white\"></i> 
										Delete
									</a>
								</td>
							</tr>
						  </tbody>
					  </table>            
					</div>
				</div><!--/span-->
			
			</div><!--/row-->";
	return $a; 
}

?>
