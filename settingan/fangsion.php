<?php
session_start();
require_once('jaka_sambung.php');
require_once('tampilan.php');
require_once('ketetapan.php');


function dropdown($sql,$sel=''){
	$opsi='';
	if (!is_array($sql)) {
		$c=q($sql);
		if(!empty($c)){
			foreach($c as $k=>$v){
				if($v['val']==$sel){
					$select="selected";
				}else{
					$select='';
				}
				@$opsi.="<option value='$v[val]' $select >$v[label]</option>";
			}
		}
	}else{#array 1 dim
		$data=$sql;
		if(empty($data)){return false;}
		foreach($data as $k=>$v){
			if($k==$sel){
				$select="selected";
			}else{
				$select='';
			}
			@$opsi.="<option value='$k' $select >$v</option>";
		}
	}
	return $opsi;
}

function detect_os(){
	$agent = $_SERVER['HTTP_USER_AGENT'];
	if(preg_match('/Linux/',$agent)) $os = 'linux';
	elseif(preg_match('/Win/',$agent)) $os = 'windows';
	elseif(preg_match('/Mac/',$agent)) $os = 'mac';
	else $os = 'unknown';
	return $os;
}

function jml_segment(){
	$a= str_replace(str_replace("\\","/",BASE_PATH),'',$_SERVER['SCRIPT_FILENAME']);
	$b=explode("/",$a);
	if (!empty($b)){
		return count($b)-1;
	}else{
		return false;
	}
}

function segment($i){
	$a= str_replace(str_replace("\\","/",BASE_PATH),'',$_SERVER['SCRIPT_FILENAME']);
	$b=explode("/",$a);
	if (!empty($b[$i])){
		return $b[$i];
	}else{
		return false;
	}
}

function loop_menu($data, $parent = 0,$jalur_ortu='0,') {
	static $i = 0;
	static $j = 0;
	//$tab = str_repeat("", $i);
	$tab = str_repeat("&nbsp;&nbsp;", $i);
	$padding=($i*10).'px';
	if (isset($data[$parent])) {
		$html = "";
		$i++;
		foreach ($data[$parent] as $v) {
			$child = loop_menu($data, $v['id']);
			if($parent==0){$url='#';}else{$url=BASE_URL."modul/$v[url]";}
			$html .= "
				<li style='padding-left:$padding;'><a class='ajax-link' href='$url'>
					<i class='".@$v['icon']."'> </i><span class='hidden-tablet'>&nbsp; $v[modul_label]</span></a>
				</li>
			";
			if ($child) {$i--;$html .= $child;}
			$html .= '';
		}
		$html .= "";
		return $html;
	} else {
		return false;
	}
}


function create_menu(){
	$ug_sess=explode(',',$_SESSION['usergroup']);
	foreach($ug_sess as $k=>$v){
		if($v == ''){ continue;}
		$ug_sql[]=" id_usergroup like '%$v%' ";
	}
	if(!empty($ug_sql)){
		$ug_sql1="and (".implode(" or ",$ug_sql).')';;
	}else{
		$ug_sql1='';
	}
	
	$db=q("select id,modul_label,url,parent,icon,id_usergroup from acl where acl='N' $ug_sql1 order by urutan");
	foreach	($db as $k=>$v){
		$menu_db[$v['parent']][]=$v;
	}
	return loop_menu($menu_db,0);
}

function tiket(){
	if(!empty($_SESSION['user'])){
		$boleh=0;$url='';
		for($i=1;$i<=jml_segment();$i++){
			$url.=segment($i)."/";
		}
		$url=substr($url,0,-1);
		$url=str_replace(".php",".aspx",$url);
		$ug=q("select id,id_usergroup from acl  where url='$url'");
		$ug_db=explode(",",$ug[0]['id_usergroup']);
		$sess_group=explode(",",$_SESSION['usergroup']);
		unset($k);unset($v);$acl1=array();
		foreach ($sess_group as $k=>$id_ug ) {
			if ($id_ug=='') {continue;}
			if (in_array($id_ug,$ug_db)) {
				$boleh=1;
				$acl=q("select modul_nama from acl where jalur_parent like '%,".$ug[0]['id']."%' and acl='Y' and id_usergroup like '%,".$id_ug.",%'");
				if (!empty($acl)) {
					unset($k);unset($v);
					foreach ($acl as $k=>$v ) {
						$acl1[]=$v['modul_nama'];
					}
				}
			}
		}
		return array_unique($acl1);
		if($boleh==0){
			header("location:".BASE_URL."/error.aspx");
		}else {
			return array_unique($acl1);
		}
	}else {
		header("location:".BASE_URL."/error.aspx");
	}
}

function breadcrumb($a=''){
	if(!empty($a)){
		$a1=explode("|",$a);
		if(!empty($a1)){
			foreach($a1 as $k1=>$v1){
				$a2=explode("=>",$v1);
				$dt[$k1]['menu']=$a2[0];
				$dt[$k1]['url']=$a2[1];
			}
		}else{
			$a1=explode("=>",$a);
			$dt[0]['url']=$a1[1];
			$dt[0]['menu']=$a1[0];
		}
		foreach	($dt as $k=>$v){
			if ($k==(count($dt)-1)){
				$delim="";
			}else{
				$delim="<span class='divider'>/</span>";
			}
			
			
			@$c.="<li>
			<a href='$v[url]'>$v[menu]</a>$delim
			</li>";
		}
		
		$b="<li>
			<a href='#'>Home</a> <span class='divider'>/</span>
		</li>
		".@$c;
	}else{
				$b="<li>
			<a href='#'>Home</a>
		</li>";
	}
	return $b;
}

function inklude($a){
	$a=str_replace("\\","/",$a);
	$file=explode('modul/',$a);
	$file1=explode('/',$file[1]);
	require_once(str_replace("_c.php","_m.php",$a));
	require_once(str_replace("_c.php","_v.php",$a));
}
function d_link($a){
	$q=q("select url from acl where modul_nama='$a'");
	return BASE_URL."modul/".$q[0]['url'];
}
function c_link($a){
	return BASE_URL."modul/$a"."_c.aspx";
}

function q($sql){
	global $link;
	
	if(strtolower(substr($sql,0,6))=='select'){
		$a=mysqli_query($link,$sql);
		while($b=mysqli_fetch_assoc($a)){
			$c[]=$b;
		}
		if(!empty($c)){
			return $c;
		}else{
			return false;
		}
	}else{
		return mysqli_query($link,$sql);
	}
}

function clean($a){
	foreach($a as $k => $v){
		$b[$k]=@mysql_real_escape_string($v);
	}
	return $b;
}

function gen_id($table){#generate id dari table tertentu
	$now=date('ymdHi');
	$dt=q("select id from $table where id like '$now%'");
	$i=count($dt)+1;
	$id=$now.$i;
	return $id;
}

function enkripsi($a){
	return base64_encode($a);
}

function dekripsi($a){
	return base64_decode($a);
	
}

function ue($param=''){#generate url enkripsi
	$ue=enkripsi($param.'^delimiter'.@$_SESSION['sess_id']);
	return $ue;
}

function ud($enc=''){#dekrip url
	$ud=dekripsi($enc);
	$ud2=explode("^delimiter",$ud);
	$ud3=explode("&",$ud2[0]);
	foreach($ud3 as $k=>$v){
		$ud4=explode("=",$v);
		$ud5[$ud4[0]]=$ud4[1];
	}
	return $ud5;
}
function sqlins($table,$data){#$data['kolom']=value;
	$kol='';$val='';
	if(empty($data)){return false;}
	foreach($data as $k=>$v){
		$kol.="$k,";
		$val.="'".mysql_real_escape_string($v)."',";
	}
	$kolom=substr($kol,0,-1);
	$value=substr($val,0,-1);
	global $link;
	return mysqli_query($link,"insert into $table ($kolom) values($value)");
}

function sqlupd($table,$data,$where){#$data['kolom']='value'; 
	$val='';
	if(empty($data)){return false;}
	foreach($data as $k=>$v){
		$val.="$k='".mysql_real_escape_string($v)."',";
	}
	$value=substr($val,0,-1);
	global $link;
	return mysqli_query($link,"update $table set $value where $where");
}

function max_id($t){
	$a=q("select max(id) as maks from $t");
	return $a[0]['maks'];
}

function notif($warna,$string){
	switch ($warna)
	{
	case 'e'://error-merah
		$alert='error';
		$title='Error';
		break;
	case 'w'://warning-kuning
		$alert='block';
		$title='Peringatan';
		break;
	case 's'://success-hijau
		$alert='success';
		$title='Sukses';
		break;
	case 'a'://alert-biru
		$alert='info';
		$title='Alert';
		break;
	}
	return "
			<div class='alert alert-".$alert."'>
				<button type='button' class='close' data-dismiss='alert'>×</button>
				<strong>$title!</strong> $string
			</div>
			";
}
?>
