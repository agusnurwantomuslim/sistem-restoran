<?php

require_once(FUNCTION_PATH);
function view_header($view=array()){
	$a="
		<html lang='en'>
<head>
	<!--
		Charisma v1.0.0

		Copyright 2012 Muhammad Usman
		Licensed under the Apache License v2.0
		http://www.apache.org/licenses/LICENSE-2.0

		http://usman.it
		http://twitter.com/halalit_usman
	-->
	<meta charset='utf-8'>
	<title>$view[title]</title>
	<meta name='viewport' content='width=device-width, initial-scale=1.0'>
	<meta name='description' content='Charisma, a fully featured, responsive, HTML5, Bootstrap admin template.'>
	<meta name='author' content='Muhammad Usman'>

	<!-- The styles -->
	<link id='bs-css' href='".BASE_URL."sekrip/css/bootstrap-simplex.css' rel='stylesheet'>
	<style type='text/css'>
	  body {
		padding-bottom: 40px;
	  }
	  .sidebar-nav {
		padding: 9px 0;
	  }
	</style>
	".@$view['css']."
	<link href='".BASE_URL."sekrip/css/bootstrap-responsive.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/charisma-app.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/jquery-ui-1.8.21.custom.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/fullcalendar.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/fullcalendar.print.css' rel='stylesheet'  media='print'>
	<link href='".BASE_URL."sekrip/css/chosen.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/uniform.default.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/colorbox.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/jquery.cleditor.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/jquery.noty.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/noty_theme_default.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/elfinder.min.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/elfinder.theme.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/jquery.iphone.toggle.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/opa-icons.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/uploadify.css' rel='stylesheet'>
	<link href='".BASE_URL."sekrip/css/style.css' rel='stylesheet'>

	<!-- The HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
	  <script src='http://html5shim.googlecode.com/svn/trunk/html5.js'></script>
	<![endif]-->

	<!-- The fav icon -->
	<link rel='shortcut icon' href='img/favicon.ico'>
		
</head>

<body>
		<!-- topbar starts -->
	<div class='navbar'>
		<div class='navbar-inner'>
			<div class='container-fluid'>
				<a class='btn btn-navbar' data-toggle='collapse' data-target='.top-nav.nav-collapse,.sidebar-nav.nav-collapse'>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
					<span class='icon-bar'></span>
				</a>
				<a class='brand' href='index.html'> <img alt='Charisma Logo' src='".BASE_URL."sekrip/img/logo20.png' /> <span>Charisma</span></a>
				<!-- user dropdown starts -->
				<div class='btn-group pull-right' >
					<a class='btn dropdown-toggle' data-toggle='dropdown' href='#'>
						<i class='icon-user'></i><span class='hidden-phone'> admin</span>
						<span class='caret'></span>
					</a>
					<ul class='dropdown-menu'>
						<li><a href='#'>Profile</a></li>
						<li class='divider'></li>
						<li><a href='login.html'>Logout</a></li>
					</ul>
				</div>
				<!-- user dropdown ends -->
				
				<div class='top-nav nav-collapse'>
					<ul class='nav'>
						<li><a href='#'>Visit Site</a></li>
						<li>
							<form class='navbar-search pull-left'>
								<input placeholder='Search' class='search-query span2' name='query' type='text'>
							</form>
						</li>
					</ul>
				</div><!--/.nav-collapse -->
			</div>
		</div>
	</div>
	<!-- topbar ends -->
		<div class='container-fluid'>
		<div class='row-fluid'>
				
			<!-- left menu starts -->
			<div class='span2 main-menu-span'>
				<div class='well nav-collapse sidebar-nav'>
					<ul class='nav nav-tabs nav-stacked main-menu'>
						<li class='nav-header hidden-tablet'>Main</li>";
						$a.=create_menu();
						$a.="
					</ul>
				</div><!--/.well -->
			</div><!--/span-->
			<!-- left menu ends -->
			
			<noscript>
				<div class='alert alert-block span10'>
					<h4 class='alert-heading'>Warning!</h4>
					<p>You need to have <a href='http://en.wikipedia.org/wiki/JavaScript' target='_blank'>JavaScript</a> enabled to use this site.</p>
				</div>
			</noscript>
			
			<div id='content' class='span10'>
			<!-- content starts -->
			

			<div>
				<ul class='breadcrumb'>
					".@$view['breadcrumb']."<!--<li>
						<a href='#'>Home</a> <span class='divider'>/</span>
					</li>
					<li>
						<a href='#'>Blank</a>
					</li>-->
				</ul>
			</div>
	";
	echo $a;
}

function view_body($view=''){
	$a=
	"
		".@$view['content']."
	";
	echo $a;
}

function view_footer($view=''){
	$a=
	"
		<!-- external javascript
	================================================== -->
	<!-- Placed at the end of the document so the pages load faster -->

	<!-- jQuery -->
	<script src='".BASE_URL."sekrip/js/jquery-1.7.2.min.js'></script>
	<!-- jQuery UI -->
	<script src='".BASE_URL."sekrip/js/jquery-ui-1.8.21.custom.min.js'></script>
	<!-- transition / effect library -->
	<script src='".BASE_URL."sekrip/js/bootstrap-transition.js'></script>
	<!-- alert enhancer library -->
	<script src='".BASE_URL."sekrip/js/bootstrap-alert.js'></script>
	<!-- modal / dialog library -->
	<script src='".BASE_URL."sekrip/js/bootstrap-modal.js'></script>
	<!-- custom dropdown library -->
	<script src='".BASE_URL."sekrip/js/bootstrap-dropdown.js'></script>
	<!-- scrolspy library -->
	<script src='".BASE_URL."sekrip/js/bootstrap-scrollspy.js'></script>
	<!-- library for creating tabs -->
	<script src='".BASE_URL."sekrip/js/bootstrap-tab.js'></script>
	<!-- library for advanced tooltip -->
	<script src='".BASE_URL."sekrip/js/bootstrap-tooltip.js'></script>
	<!-- popover effect library -->
	<script src='".BASE_URL."sekrip/js/bootstrap-popover.js'></script>
	<!-- button enhancer library -->
	<script src='".BASE_URL."sekrip/js/bootstrap-button.js'></script>
	<!-- accordion library (optional, not used in demo) -->
	<script src='".BASE_URL."sekrip/js/bootstrap-collapse.js'></script>
	<!-- carousel slideshow library (optional, not used in demo) -->
	<script src='".BASE_URL."sekrip/js/bootstrap-carousel.js'></script>
	<!-- autocomplete library -->
	<script src='".BASE_URL."sekrip/js/bootstrap-typeahead.js'></script>
	<!-- tour library -->
	<script src='".BASE_URL."sekrip/js/bootstrap-tour.js'></script>
	<!-- library for cookie management -->
	<script src='".BASE_URL."sekrip/js/jquery.cookie.js'></script>
	<!-- calander plugin -->
	<script src='".BASE_URL."sekrip/js/fullcalendar.min.js'></script>
	<!-- data table plugin -->
	<script src='".BASE_URL."sekrip/js/jquery.dataTables.min.js'></script>
	".@$view['js']."
	<!-- chart libraries start -->
	<script src='".BASE_URL."sekrip/js/excanvas.js'></script>
	<script src='".BASE_URL."sekrip/js/jquery.flot.min.js'></script>
	<script src='".BASE_URL."sekrip/js/jquery.flot.pie.min.js'></script>
	<script src='".BASE_URL."sekrip/js/jquery.flot.stack.js'></script>
	<script src='".BASE_URL."sekrip/js/jquery.flot.resize.min.js'></script>
	<!-- chart libraries end -->

	<!-- select or dropdown enhancer -->
	<script src='".BASE_URL."sekrip/js/jquery.chosen.min.js'></script>
	<!-- checkbox, radio, and file input styler -->
	<script src='".BASE_URL."sekrip/js/jquery.uniform.min.js'></script>
	<!-- plugin for gallery image view -->
	<script src='".BASE_URL."sekrip/js/jquery.colorbox.min.js'></script>
	<!-- rich text editor library -->
	<script src='".BASE_URL."sekrip/js/jquery.cleditor.min.js'></script>
	<!-- notification plugin -->
	<script src='".BASE_URL."sekrip/js/jquery.noty.js'></script>
	<!-- file manager library -->
	<script src='".BASE_URL."sekrip/js/jquery.elfinder.min.js'></script>
	<!-- star rating plugin -->
	<script src='".BASE_URL."sekrip/js/jquery.raty.min.js'></script>
	<!-- for iOS style toggle switch -->
	<script src='".BASE_URL."sekrip/js/jquery.iphone.toggle.js'></script>
	<!-- autogrowing textarea plugin -->
	<script src='".BASE_URL."sekrip/js/jquery.autogrow-textarea.js'></script>
	<!-- multiple file upload plugin -->
	<script src='".BASE_URL."sekrip/js/jquery.uploadify-3.1.min.js'></script>
	<!-- history.js for cross-browser state change on ajax -->
	<script src='".BASE_URL."sekrip/js/jquery.history.js'></script>
	<!-- application script for Charisma demo -->
	<script src='".BASE_URL."sekrip/js/charisma.js'></script>
	
		
</body>
</html>

	";
	echo $a;
}


function view_layout($view){
	$a=view_header($view);
	$a.=view_body($view);
	$a.=view_footer($view);
	echo $a;
}
?>
