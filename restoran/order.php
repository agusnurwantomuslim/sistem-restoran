<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8" />
	<title>Order</title>
	<link rel="stylesheet" type="text/css" href="http://localhost/restoran/css/style.css">
</head>
<body>
<div id="wraper">
	<div id="sidebar" class="left">
		<h2>Logo restoran</h1>
		<div class="header">
			Pesanan Anda
		</div>
		<table width="97%" style="margin:auto" border="1px" cellspacing="0">
			<tr background="#bbb">
				<th colspan='3'>Menu</th>
				<th>Harga</th>
				<th>Opsi</th>
			</tr>
			<?php
				$harga = 30000;
				$menu = 'Nasi Goreng';
				$size = 10;
				$total = 0;
				for( $i=1; $i<=$size; $i++ ){
					echo "<tr>
							<td><input type='checkbox' name='' value='' /></td>
							<td>$i</td>
							<td>$menu</td>
							<td>Rp $harga</td>
							<td> E D </td>
						</tr>"; 
					$total = $total + $harga;
				}
			?>
			<tr>
				<th colspan='3'>Total</th>
				<th colspan='2' width="45%">Rp <?php echo $total; ?></th>
			</tr>
		</table>
		<a href="#" class="submit right">Pesan</a>
	</div>
	<div id="content" class="right">
		<div class='title left'>Menu Spesial</div>
		<div class='pagination right'>page 1 of 2: 1 2</div>
		<div class='pagination right'>
			<input type='text' name='' value='' id='search' /> 
			<a href="#">Search</a>
		</div>
		<div class="clear"></div>
		<?php
			$size = 15;
			for( $i=1; $i<=$size; $i++ ){
				echo "<div class='menu'>$i</div>"; 
			}
		?>
		<div class="clear"></div>
		<div class="footer">
			<a href="#">Spesial </a>
			<a href="#">Seafood </a>
			<a href="#">Nasi </a>
			<a href="#">Sayuran </a>
			<a href="#">Buah </a>
			<a href="#">Camilan </a>
			<a href="#">Minuman </a>
		</div>
	</div>
	<div class="clear"></div>
</div>
</body>
</html>
