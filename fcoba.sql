-- phpMyAdmin SQL Dump
-- version 3.3.9
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Oct 09, 2013 at 03:59 AM
-- Server version: 5.5.8
-- PHP Version: 5.3.5

SET SQL_MODE="NO_AUTO_VALUE_ON_ZERO";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `fcoba`
--

-- --------------------------------------------------------

--
-- Table structure for table `acl`
--

CREATE TABLE IF NOT EXISTS `acl` (
  `id` int(3) NOT NULL AUTO_INCREMENT,
  `modul_nama` varchar(128) NOT NULL,
  `modul_label` varchar(254) NOT NULL,
  `parent` int(3) NOT NULL,
  `jalur_parent` text NOT NULL,
  `url` text NOT NULL,
  `acl` enum('Y','N') NOT NULL,
  `id_usergroup` longtext NOT NULL,
  `id_user` longtext NOT NULL,
  `urutan` int(3) NOT NULL,
  `lefel` int(2) NOT NULL,
  `menugroup` varchar(64) NOT NULL,
  `icon` varchar(32) NOT NULL DEFAULT 'icon-circle-arrow-right',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=latin1 AUTO_INCREMENT=31 ;

--
-- Dumping data for table `acl`
--

INSERT INTO `acl` (`id`, `modul_nama`, `modul_label`, `parent`, `jalur_parent`, `url`, `acl`, `id_usergroup`, `id_user`, `urutan`, `lefel`, `menugroup`, `icon`) VALUES
(6, 'setting', 'Setting', 0, '0', 'setting/setting_c.aspx', 'N', ',1,', '', 3, 1, '', 'icon-cog'),
(16, 'user_login', 'User and Login', 6, '0,6', 'setting/user_login/user_login_c.aspx', 'N', ',1,', '', 0, 2, '', 'icon-user'),
(17, 'home', 'Home', 0, '0', 'home/home_c.aspx', 'N', ',2,3,', '', 1, 1, '', 'icon-home'),
(18, 'acl', 'Access Control List', 6, '0,6', 'setting/acl/acl_c.aspx', 'N', ',1,', '', 0, 2, '', 'icon-lock'),
(27, 'service', 'Service', 0, '0', 'service/service_c.aspx', 'N', ',1,2,', '', 0, 1, '', 'icon-circle-arrow-right'),
(28, 'add', '', 27, '0,27', '', 'Y', ',1,7,', '', 0, 2, '', 'icon-lock'),
(29, 'edit', '', 27, '0,27', 'service/edit/edit_c.aspx', 'Y', ',7,1,', '', 0, 2, '', 'icon-circle-arrow-right'),
(30, 'delete', '', 27, '0,27', '', 'Y', ',1,', '', 0, 2, '', 'icon-lock');

-- --------------------------------------------------------

--
-- Table structure for table `history_banned`
--

CREATE TABLE IF NOT EXISTS `history_banned` (
  `id` int(13) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `tgl` datetime NOT NULL,
  `alasan` int(11) NOT NULL,
  `tipe` enum('Banned','Reactive') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `history_banned`
--


-- --------------------------------------------------------

--
-- Table structure for table `log_activity`
--

CREATE TABLE IF NOT EXISTS `log_activity` (
  `session_id` varchar(128) NOT NULL,
  `uid` varchar(64) NOT NULL,
  `tgl` datetime NOT NULL,
  `browser` varchar(64) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `os` varchar(16) NOT NULL,
  `aksi` varchar(64) NOT NULL,
  `ket` text NOT NULL,
  PRIMARY KEY (`session_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_activity`
--


-- --------------------------------------------------------

--
-- Table structure for table `log_b4_login`
--

CREATE TABLE IF NOT EXISTS `log_b4_login` (
  `id` varchar(13) NOT NULL,
  `uid` varchar(128) NOT NULL,
  `passwd` varchar(32) NOT NULL,
  `ip` varchar(20) NOT NULL,
  `browser` varchar(64) NOT NULL,
  `tgl` datetime NOT NULL,
  `os` varchar(16) NOT NULL,
  `fail` enum('Fail','Succes') NOT NULL,
  `captcha` varchar(16) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `log_b4_login`
--

INSERT INTO `log_b4_login` (`id`, `uid`, `passwd`, `ip`, `browser`, `tgl`, `os`, `fail`, `captcha`) VALUES
('13092902582', 'asd', 'asd', '192.168.1.16', '', '2013-09-29 02:58:00', '', 'Fail', 'asd'),
('13092902592', 'asd', 'asd', '192.168.1.16', '', '2013-09-29 02:59:00', '', 'Fail', 'asd'),
('13092903022', 'root', 'root', '192.168.1.16', '', '2013-09-29 03:02:00', '', '', 'rebless'),
('13093000482', 'root', 'rooteuryon', '127.0.0.1', '', '2013-09-30 00:48:00', '', 'Fail', ''),
('13093000492', 'root', 'root', '127.0.0.1', '', '2013-09-30 00:49:00', '', '', 'formwork'),
('13093000572', 'root', 'rooteuryon', '127.0.0.1', '', '2013-09-30 00:57:00', '', 'Fail', ''),
('13093002382', '', '', '127.0.0.1', '', '2013-09-30 02:38:00', '', 'Fail', ''),
('13093002392', 'root', 'root', '127.0.0.1', '', '2013-09-30 02:39:00', '', '', 'labium'),
('13100101102', 'root', 'root', '127.0.0.1', '', '2013-10-01 01:10:00', '', '', 'nithing'),
('13100203042', 'root', 'root', '127.0.0.1', '', '2013-10-02 03:04:00', '', '', 'urate'),
('13100302322', 'sdf', '', '127.0.0.1', '', '2013-10-03 02:32:00', '', 'Fail', ''),
('13100302432', 'sdf', '', '127.0.0.1', '', '2013-10-03 02:43:00', '', 'Fail', ''),
('13100302442', 'asd', 'ad', '127.0.0.1', '', '2013-10-03 02:44:00', '', 'Fail', 'ravison'),
('13100412152', 'root', 'root', '127.0.0.1', '', '2013-10-04 12:15:00', '', 'Fail', 'polliria'),
('13100412162', 'root', 'root', '127.0.0.1', '', '2013-10-04 12:16:00', '', 'Fail', 'cupcake'),
('13100807342', 'root', 'root', '127.0.0.1', '', '2013-10-08 07:34:00', '', '', 'firefall'),
('13100811142', 'root', 'root', '127.0.0.1', '', '2013-10-08 11:14:00', '', '', 'carried'),
('13100811172', 'root', 'root', '127.0.0.1', '', '2013-10-08 11:17:00', '', '', 'wabunga'),
('13100811582', 'root', 'root', '127.0.0.1', '', '2013-10-08 11:58:00', '', '', 'upgorge'),
('13100903252', 'sdfsd', 'fsdfsdf', '127.0.0.1', '', '2013-10-09 03:25:00', '', 'Fail', ''),
('13100903312', 'root', 'root', '127.0.0.1', '', '2013-10-09 03:31:00', '', '', 'yuckel');

-- --------------------------------------------------------

--
-- Table structure for table `ref_bahasa`
--

CREATE TABLE IF NOT EXISTS `ref_bahasa` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `bahasa` varchar(16) NOT NULL,
  `kata` text NOT NULL,
  `translasi` text NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 AUTO_INCREMENT=1 ;

--
-- Dumping data for table `ref_bahasa`
--


-- --------------------------------------------------------

--
-- Table structure for table `ref_grup_pengguna`
--

CREATE TABLE IF NOT EXISTS `ref_grup_pengguna` (
  `id` int(3) NOT NULL,
  `nama` varchar(64) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_grup_pengguna`
--

INSERT INTO `ref_grup_pengguna` (`id`, `nama`, `aktif`) VALUES
(1, 'admin', 'Y'),
(2, 'agen', 'Y'),
(3, 'mitra', 'Y'),
(4, 'subagen', 'Y'),
(5, 'submitra', 'Y'),
(6, 'finance', 'Y'),
(7, 'cs', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `ref_konfig`
--

CREATE TABLE IF NOT EXISTS `ref_konfig` (
  `meta_key` varchar(32) NOT NULL,
  `meta_val` longtext NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  PRIMARY KEY (`meta_key`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_konfig`
--

INSERT INTO `ref_konfig` (`meta_key`, `meta_val`, `aktif`) VALUES
('bahasa', 'english', 'Y'),
('default_usergroup', '1', 'Y'),
('login_fb', 'Y', 'Y'),
('login_tw', 'Y', 'Y'),
('user_activated', 'no_confirm', 'Y');

-- --------------------------------------------------------

--
-- Table structure for table `ref_pengguna`
--

CREATE TABLE IF NOT EXISTS `ref_pengguna` (
  `uid` varchar(64) NOT NULL,
  `pass` varchar(32) NOT NULL,
  `nickname` varchar(64) NOT NULL,
  `fullname` varchar(128) NOT NULL,
  `gender` enum('M','F') NOT NULL,
  `email` varchar(128) NOT NULL,
  `usergroup_id` varchar(32) NOT NULL,
  `fb` varchar(128) NOT NULL,
  `tw` varchar(128) NOT NULL,
  `reg_tgl` datetime NOT NULL,
  `reg_ip` varchar(20) NOT NULL,
  `last_login` datetime NOT NULL,
  `reg_from` enum('fb','tw','sys') NOT NULL,
  `forgot_pass_tanya` text NOT NULL,
  `forgot_pass_jawab` text NOT NULL,
  `set_bahasa` varchar(16) NOT NULL,
  `zona_waktu` varchar(64) NOT NULL,
  `aktif` enum('Y','N') NOT NULL,
  PRIMARY KEY (`uid`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `ref_pengguna`
--

INSERT INTO `ref_pengguna` (`uid`, `pass`, `nickname`, `fullname`, `gender`, `email`, `usergroup_id`, `fb`, `tw`, `reg_tgl`, `reg_ip`, `last_login`, `reg_from`, `forgot_pass_tanya`, `forgot_pass_jawab`, `set_bahasa`, `zona_waktu`, `aktif`) VALUES
('root', '63a9f0ea7bb98050796b649e85481845', 'usman', 'usman rubiantoro', 'M', '', ',1,2,', '', '', '0000-00-00 00:00:00', '', '0000-00-00 00:00:00', '', '', '', '', '', 'Y');
